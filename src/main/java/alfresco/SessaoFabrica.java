package alfresco;

import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;

public class SessaoFabrica {
	
	private Session sessao;
	
	public SessaoFabrica() {
		
		Map<String, String> parameter = new HashMap<String, String>();
		parameter.put(SessionParameter.USER, "thiago.ferreira");
		parameter.put(SessionParameter.PASSWORD, "abc,123");
		parameter.put(SessionParameter.ATOMPUB_URL, "http://gedoc.fortaleza.ce.gov.br/alfresco/cmisatom");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		parameter.put(SessionParameter.REPOSITORY_ID, "2f35c6c5-c321-44ce-a889-37c06580fd61");
		parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");//parametro que permite trabalhar com alfresco extensions
		
		SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
		this.sessao = sessionFactory.createSession(parameter);
		
	}
	
	public Session getSessao() {
		return this.sessao;
	}
}
