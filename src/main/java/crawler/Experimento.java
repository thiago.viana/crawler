package crawler;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Experimento {

	public static void main(String[] args) throws Exception {
		
		final String DIRETORIO_RAIZ = "C:\\Users\\Iplanfor\\Downloads\\teste";
		
		final String DOMINIO = "http://legislacao.fortaleza.ce.gov.br";
		
		Document doc = Jsoup.connect(DOMINIO + "/index.php/Leis_Complementares_2014").get();
		
		Elements links = doc.select("table.wikitable a");
		
		for (Element link : links) {
			if (link.attr("href").endsWith(".pdf")) {
				continue;
			} else {
				Document doc2 = Jsoup.connect(DOMINIO + link.attr("href")).get();
				String nomeHtml = doc2.getElementById("firstHeading").text();
				FileUtils.writeStringToFile(new File(DIRETORIO_RAIZ +  "\\" + nomeHtml + ".html"), 
						doc2.getElementById("mw-content-text").toString(), 
						"ISO-8859-1");
				
				String htmlOrigem = DIRETORIO_RAIZ + "\\" + nomeHtml + ".html";
				String pdfDestino = DIRETORIO_RAIZ + "\\" + nomeHtml + ".pdf";
				
				converterHtmlParaPdf(htmlOrigem, pdfDestino);
							
			}
		}

	}
	
	public static void converterHtmlParaPdf(String htmlOrigem, String pdfDestino) throws IOException{
		String pathWkhtmltopdf = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
		ProcessBuilder pb = new ProcessBuilder(pathWkhtmltopdf, htmlOrigem, pdfDestino);
        pb.redirectErrorStream(true);
        pb.start();
	}

}
