package crawler;


/**
 * @author Thiago Ferreira
 * 
 * Essa � a classe principal. Ela que chama os crawlers e no final
 * elimina os arquivos .html
 */
public class Crawler {

	public static void main(String[] args) throws Exception {
		
		new CrawlerLegislacaoCultural().navegacaoStart();
		new CrawlerOutrasLeis().navegacaoStart();
		new HtmlDelete().entrarNaPasta("C:\\Users\\Iplanfor\\Downloads\\PGM Legisla��o\\");

	}

}
