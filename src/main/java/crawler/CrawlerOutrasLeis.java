package crawler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Thiago Ferreira
 * 
 * Navega pelas leis do Portal da PGM, exceto Legislação Cultural,
 * e faz os download das leis.
 */
public class CrawlerOutrasLeis {
	
	
	final String DIRETORIO_RAIZ = "C:\\Users\\Iplanfor\\Downloads\\PGM Legislação\\";
	
	final String DOMINIO = "http://legislacao.fortaleza.ce.gov.br";
	
	int counter = 0;
	
	public static void main(String[] args) throws Exception {
		
		new CrawlerOutrasLeis().navegacaoStart();
	}
	
	public void navegacaoStart() throws Exception {
		
		Document doc = Jsoup.connect(DOMINIO + "/index.php/P%C3%A1gina_principal").get();
		
		Elements links = doc.select("div#cx-link a");
		
		for (Element link : links) {
			if (link.text().equals("Legislação Cultural"))
				continue;
			
			System.out.println(link.text());
			Document doc1 = Jsoup.connect(DOMINIO + link.attr("href")).get();
			Elements links1 = doc1.select("div#cx-link-interno a");
			
			for (Element link1 : links1) {
				System.out.println("\t" + link1.text());
				if (link1.attr("title").contains("inexistente"))
					continue;
				Document doc2 = Jsoup.connect(DOMINIO + link1.attr("href")).get();
				Elements links2 = doc2.select("table.wikitable a");
				
				for (Element link2 : links2) {
					if (link2.attr("title").contains("inexistente"))
						continue;
					
					if (link2.attr("href").endsWith(".pdf")) {
						
						String nomePdf = link2.attr("href").substring(link2.attr("href").lastIndexOf("/") + 1);
						String url = DOMINIO + link2.attr("href");
						String localOndeSeraSalvo = DIRETORIO_RAIZ + link.text() + "\\" + link1.text() + "\\" + nomePdf;
						downloadDoDocumentoEmPdf(url, localOndeSeraSalvo);
						
					}
					else {
						System.out.println("\t\tHTML: " + link2.text() + " - " + link2.attr("href"));
						Document doc3 = Jsoup.connect(DOMINIO + link2.attr("href")).get();
						
						String nome = doc3.getElementById("firstHeading").text().toUpperCase();
						String localOndeSeraSalvo = DIRETORIO_RAIZ + link.text() + "\\" + link1.text() + "\\" + nome + ".html";
						
						downloadDoDocumentoEmHtml(localOndeSeraSalvo, doc3);
						
						String htmlOrigem = DIRETORIO_RAIZ + link.text() + "\\" + link1.text() + "\\" + nome + ".html";
						String pdfDestino = DIRETORIO_RAIZ + link.text() + "\\" + link1.text() + "\\" + nome + ".pdf";
						
						converterHtmlParaPdf(htmlOrigem, pdfDestino);
					}
				}
			}
		}
		System.out.println("Total de Falhas: " + counter);
	}
	
	public void converterHtmlParaPdf(String htmlOrigem, String pdfDestino) throws IOException{
		String pathWkhtmltopdf = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
		ProcessBuilder pb = new ProcessBuilder(pathWkhtmltopdf, htmlOrigem, pdfDestino);
        pb.redirectErrorStream(true);
        pb.start();
	}
	
	public void downloadDoDocumentoEmPdf(String url, String localOndeSeraSalvo) {
		try {
			FileUtils.copyURLToFile(new URL(url),new File(localOndeSeraSalvo));
			System.out.println("\t\tPDF: " + url);
		} catch(FileNotFoundException e) {
			System.out.println("\t\tFALHOU: " + url);
			counter++;
		} catch(IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void downloadDoDocumentoEmHtml(String localOndeSeraSalvo, Document doc) {
		try {
			FileUtils.writeStringToFile(new File(localOndeSeraSalvo), 
					doc.getElementById("mw-content-text").toString(), 
					"ISO-8859-1");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

}
