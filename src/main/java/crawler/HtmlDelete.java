package crawler;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

/**
 * @author Thiago Ferreira
 * 
 * Essa pasta navega pela estrutura de pastas criadas pelos crawlers
 * e vai excluindo os arquivos .html
 * 
 */
public class HtmlDelete {
	
	final static String CAMINHO_RAIZ = "C:\\Users\\Iplanfor\\Downloads\\PGM Legislação\\";
	
	public static void main(String[] args) throws Exception {
		
		new HtmlDelete().entrarNaPasta(CAMINHO_RAIZ);
	}
	
	public void entrarNaPasta(String caminho) throws Exception {
		
		System.out.println(caminho);
		File pasta = new File(caminho);
		File[] files = pasta.listFiles();
		
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && !files[i].isHidden()) {				
				if (files[i].getName().endsWith(".html")) {
					System.out.println(files[i].getAbsolutePath());
					deletarArquivoHtml(new File(files[i].getAbsolutePath()));
				}
					
			} else {
				entrarNaPasta(files[i].getAbsolutePath());
			}
			
		}
	}
	
	public void deletarArquivoHtml(File arquivo) throws IOException {
		FileUtils.forceDelete(arquivo);
	}

}
